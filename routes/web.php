<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
    //return json_encode(['name' => 'Uasya', 'age' => 30]);
    return view('tt');
});

Route::group(['prefix' => 'api', 'namespace' => 'API', 'middleware' => 'cors' ], function () {
    //Infomedia
    Route::post('/infomedia/sendPhone', [ 'as' => 'send_phone', 'uses' => 'InfomediaController@sendPhone' ]);
    Route::post('/infomedia/checkPin', [ 'as' => 'check_pin', 'uses' => 'InfomediaController@checkPin' ]);
    Route::post('/infomedia/getInfomediaData', [ 'as' => 'get_infomedia_data', 'uses' => 'InfomediaController@getInfomediaData' ]);
    Route::post('/infomedia/sendAllDataToInfomedia', [ 'as' => 'send_infomedia_data', 'uses' => 'InfomediaController@sendAllDataToInfomedia' ]);
    Route::get('/infomedia/getNotification', [ 'as' => 'get_notification', 'uses' => 'InfomediaController@getNotification' ]);
    Route::post('/infomedia/getNotification', [ 'uses' => 'InfomediaController@getNotification' ]);
    Route::post('/infomedia/createLicense', [ 'uses' => 'InfomediaController@createLicense' ]);
});

Route::get('/test', ['uses' => 'TestController@index']);

//Route::post('/api/infomedia/sendPhone', ['middleware' => 'cors', 'uses' => 'API\InfomediaController@sendPhone']);
