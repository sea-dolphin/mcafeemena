<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

use App\Models\MongoSession;
//use App\Models\MongoPaymentBh;

use App\Http\Controllers\CommonController;

class InfomediaController extends Controller
{
    /*
     * Return Infomedia data
     */
    public function getInfomediaData()
    {
        $aData = [
            'PGW_COMPANY' => config('paymentsData.bh.infomedia.PGW_COMPANY'),
            'IM_PI' => config('paymentsData.bh.infomedia.IM_PI'),
            'IM_PF' => config('paymentsData.bh.infomedia.IM_PF'),
            'MD5Hash' => md5(config('paymentsData.bh.infomedia.PGW_PASS') . config('paymentsData.bh.infomedia.IM_SECRET')),
        ];
        
        return json_encode($aData);
    }
    
    /*
     * Send phone and get PIN
     */
    public function sendPhone(Request $request)
    {
        $aResponse = [ 'success' => false ];
        $strPhone = $request->input('phone');
        $nSmsId = rand(10000000, 409237000);
        $nPin = rand(1000, 9999);
        $aData = [
            'MNC'     => '02',
            'MCC'     => '424',
            'smsId'   => $nSmsId,
            'SID'     => config('paymentsData.bh.infomedia.IM_SID_ETISALAT'),
            'U'       => config('paymentsData.bh.infomedia.PGW_COMPANY'),
            'P'       => config('paymentsData.bh.infomedia.PGW_PASS'),
            'MD5Hash' => md5(config('paymentsData.bh.infomedia.IM_HERMES_PASS') . config('paymentsData.bh.infomedia.IM_HERMES_SECRET')),
            'MX'      => $strPhone,
            'AID' => 177,
            'PID' => 92000180,
        ];
        //make correct MD5Hash
        $aData['MD5Hash'] = implode("-", str_split($aData['MD5Hash'], 2));
        
        //Mongo Insert
//        MongoSession::create([
//            'phone' => $strPhone,
//            'smsId' => $nSmsId,
//            'pin' => $nPin,
//            //'created_at' => Carbon::now(),
//        ]);
        
        $oSessions = new MongoSession;
        $oSessions->phone = $strPhone;
        $oSessions->smsId = $nSmsId;
        $oSessions->pin = $nPin;
        $oSessions->save();
        
        $strTextSms = 
                'FreeMsg: من أجل تأكيد 
                اشتراكك في Mcafeemena،, 
                يرجى إدخال كود التفعيل: ' 
                . $nPin . ' 
                '. 
                ' تحتاج إلى المساعدة؟ اتصل بخدمة 
                دعم العملاء مجانا على: 0386010107';
        
        $xmlData = '<?xml version="1.0" encoding="utf-8"?>
                <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
                  <soap12:Body>
                    <SendMessageWithCampaign xmlns="http://tempuri.org/">
                      <Username>'. config('paymentsData.bh.infomedia.PGW_COMPANY') .'</Username>
                      <MD5Hash>'. $aData['MD5Hash'] .'</MD5Hash>
                      <SMSMessage>' . $strTextSms . '</SMSMessage>
                      <MSISDNList>
                        <MSISDNDetails>
                          <MSISDN>'.$aData['MX'].'</MSISDN>
                          <MCC>'.$aData['MCC'].'</MCC>
                          <MNC>'.$aData['MNC'].'</MNC>
                        </MSISDNDetails>
                      </MSISDNList>
                      <UserMsgID>'. $aData['smsId'] .'</UserMsgID>
                      <ScheduleFor>00000000000000</ScheduleFor>
                      <CampaignID>' . config('paymentsData.bh.infomedia.IM_CampaignID') . '</CampaignID>
                    </SendMessageWithCampaign>
                  </soap12:Body>
                </soap12:Envelope>';
        
        $aHeaders = [
            "Content-type: text/xml; charset=\"utf-8\"",
            "Accept: text/xml",
            "SOAPAction: http://tempuri.org/SendMessageWithCampaign", 
            "Content-length: ".strlen($xmlData),
        ];
        
        // Log what we send
        Log::info('InfoMedia user data send request for get Pin: ' . print_r($request->all(), true));
        Log::info('InfoMedia xml to send for get Pin: ' . print_r($aHeaders, true) .  $xmlData);
        
        $xmlResponse = $this->curlGetPin($aData, $xmlData, $aHeaders);
        
        // Log response
        Log::info('InfoMedia get data for Pin: ' . print_r($xmlResponse, true));
        
        // Get info
        $response_xml1 = str_replace("<soap:Body>","", $xmlResponse);
        $response_xml2 = str_replace("</soap:Body>","", $response_xml1);
        $response_xml3  = simplexml_load_string($response_xml2);
        $oResultXml = $response_xml3->SendMessageWithCampaignResponse->SendMessageWithCampaignResult;

        // status 00 - success
        if( $oResultXml->Status == '00' )
        {
            $aResponse['success'] = true;
        }

        return json_encode($aResponse);
    }
    
    public function checkPin(Request $request)
    {
        $aResponse = [ 'success' => false ];
        
        $oSessions = MongoSession::where('phone', $request->input('phone'))
                ->where('pin', intval($request->input('pin')))
                ->first();
        
        if( empty($oSessions->_id) )
        {
            $aResponse['message'] = 'Incorrect Pin';
            MongoSession::where('phone', $request->input('phone'))->delete();
            
            return $aResponse;
        }
        else
        {
            $aResponse['mx'] = empty($request->input('mx')) ? '' : $request->input('mx'); //?
            $aResponse['success'] = true;
        }
        
        MongoSession::where('phone', $request->input('phone'))->delete();
        
        return json_encode($aResponse);
    }
    
    /*
     * Send data to Infomedia and subscribe user
     */
    public function sendAllDataToInfomedia(Request $request)
    {
        //return json_encode([ 'success' => true ]);
        $aResponse = [ 'success' => false ];
        
        $aInitData = [
            'U'   => config('paymentsData.bh.infomedia.PGW_COMPANY'), // env('PGW_COMPANY', ''),
            'P'   => config('paymentsData.bh.infomedia.PGW_PASS'), // env('PGW_PASS', ''),
            'SID' => config('paymentsData.bh.infomedia.IM_SID'), // env('IM_SID', ''),
            'MX'  => $request->input('phone'),
            'MNC' => empty($request->input('mnc')) ? '99' : $request->input('mnc'), // Input::get('mnc', ''),
            'MCC' => empty($request->input('mcc')) ? '234' : $request->input('mcc'), // Input::get('mcc', ''),
            'AID' => 177,
            'PID' => 92000180,
        ];
        
        $nUserId = rand(10000000, 409237000);
        
        Log::info('Infomedia Get subscription for ' . $request->input('phone') . ' (init data): ' . print_r($aInitData, true));
        
        $oResultSendData = $this->curlSendData($aInitData, config('paymentsData.bh.infomedia.SUBSCRIPTION'));
        
        Log::info('Infomedia Get subscription for ' . $request->input('phone') . ' (result):  ' . print_r($oResultSendData, true));
        
        if( !empty($oResultSendData->Status) && $oResultSendData->Status == 'Success' )
        {
            $aResponse['success'] = true;
        }
        elseif( !empty($oResultSendData->User->Subscribed) && ($oResultSendData->User->Subscribed == 'True' ) )
        {
            $aResponse['success'] = true;
        }
        
        return json_encode($aResponse);
    }
    
    /*
     * Get notification from infomedia
     */
    public function getNotification(Request $request)
    {
        if ( !empty($request->input('key')) )
        {
            $paymentModel = 'App\Models\\' . config('paymentsData.bh.payment_model');
            $oPaymants = new $paymentModel;
            $oPaymants->payment_name = config('paymentsData.bh.payment_name');
            $oPaymants->msisdn = $request->input('msisdn');
            $oPaymants->transactionId = $request->input('transactionId');
            $oPaymants->status = $request->input('status');
            $oPaymants->content = json_encode( [ $request->all() ] ); // print_r($request->all(), true);
            $oPaymants->save();
            
            return json_encode( ['success' => true] );
        }
        
        return json_encode( ['success' => true] );
    }
    
    /*
     * Send data to License Api: create license method
     */
    public function createLicense(Request $request)
    {
        $aData = [
            'licenceID' => config('paymentsData.bh.license_api.licenceID'),
            'emailAddress' => $request->input('emailAddress'),
            'MSISDN' => $request->input('phone'),
            'Username' => config('paymentsData.bh.license_api.Username'),
            'Password' => config('paymentsData.bh.license_api.Password'),
        ];
        
        $mResult = CommonController::sendCurlData(config('paymentsData.bh.license_api.url_test'), $aData, 'form');
        
        return json_encode( ['success' => true, 'message' => print_r($mResult, true) ] );
    }
    
    /*
     * CURL for send Phone and getPIN
     */
    public function curlGetPin($aData, $xmlData, $aHeaders)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, config('paymentsData.bh.infomedia.URL_GET_PIN'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $aData['U'].":".$aData['P']); // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlData); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $aHeaders);
        $xmlResponse = curl_exec($ch);
        curl_close($ch);
        
        return $xmlResponse;
    }
    
    /*
     * Curl Send JSON data
     */
    public function curlSendData($aData, $strUrl)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_URL, $strUrl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [ 'Content-Type: application/json;charset=UTF-8' ]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($aData, JSON_UNESCAPED_SLASHES));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50);
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        $response = curl_exec($ch);
        curl_close($ch);
        
        return $response;
    }
}
