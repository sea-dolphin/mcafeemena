<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommonController extends Controller
{
    public static function sendCurlData($strUrl, $aData, $strType)
    {
//        $aTypes = [
//            'json' => 'Content-Type: application/json;charset=UTF-8'
//        ];
        
        $ch   = curl_init();
        curl_setopt($ch, CURLOPT_POST, $aData);
        curl_setopt($ch, CURLOPT_URL, $strUrl);
        
        if ( $strType == 'json' )
        {
            curl_setopt($ch, CURLOPT_HTTPHEADER, [ 'Content-Type: application/json;charset=UTF-8' ] );
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($aData, JSON_UNESCAPED_SLASHES));
        }
        
        if ( $strType == 'form' )
        {
            $strFromData = '';
            
            foreach($aData as $key => $strVal)
            {
                $strFromData .= $key.'='.$strVal.'&';
            }
            
            curl_setopt($ch, CURLOPT_POSTFIELDS, $strFromData);
        }
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        
        $mResult = curl_exec($ch);
        
        //$err = curl_error($ch);
        //$info = curl_getinfo($ch);
        
        curl_close($ch);
        
        return $mResult;
    }
}
