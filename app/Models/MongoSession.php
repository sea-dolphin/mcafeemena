<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;

class MongoSession extends Eloquent 
{
    protected $connection = 'mongodb';
    protected $collection = 'sessions';
    
    /*
     * Collection structure:
     * 
     * phone: string            # Phone number
     * smsId: int               # SMS id, generate by merchant: rand(..., ...)
     * pin: int                 # Pin code for phone number confirmation
     * updated_at: DateTime     # ISODate("2017-11-13T16:39:40Z")
     * created_at: DateTime     # ISODate("2017-11-13T16:39:40Z")
     */
}
