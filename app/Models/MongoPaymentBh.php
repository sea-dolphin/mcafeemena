<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;

class MongoPaymentBh extends Eloquent 
{
    protected $connection = 'mongodb';
    protected $collection = 'payments_bh';
    
    /*
     * Collection structure:
     * 
     * payment_name: string             # Name of the payment system for structuring the data
     * msisdn: string/int               # Phone number
     * transactionId: int               # Transaction ID by payment system
     * status: string                   # Payment status
     * content: string                  # All content (parameters) from the payment system
     * updated_at: DateTime             # ISODate("2017-11-13T16:39:40Z")
     * created_at: DateTime             # ISODate("2017-11-13T16:39:40Z")
     */
}
